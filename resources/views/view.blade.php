@extends('layouts.app')

@section('content')
@if(session('success'))
 <div class="alert alert-success" role="alert">
 	{{ session('success')}}
 	
 </div>
 @endif
 <a href="{{ route('crud.create')}}" class="btn btn-light">Create Task</a>

 <div class="col-sm-12">
 	<div class="row">
 		<table class="table table-striped">
 			<thead>
 				<th>Id</th>
 				<th>Title</th>
 				<th>Description</th>
 				<th>Task tag</th>
 				<th>Edit</th>
 				<th>Delete</th>
 			</thead>
 			<tbody>
 				@if($cruds)
 				@foreach($cruds as $crud)
 				<tr>
 				<td>{{ $crud->id }}</td>
 				<td>{{ $crud->title }}</td>
 				<td>{{ $crud->description }}</td>
 				<td>{{ $crud->tags }}</td>
 				<td><a href="{{ route('crud.edit', $crud->id)}}" class="btn btn-light">Edit</a></td>
 				<td>
 					<form method="post" action="{{ route('crud.destroy',$crud->id)}}">
 						@csrf
 						@method('DELETE')
 						<button class="btn btn-danger" type="sumbit">Delete</button>
 					</form>
 					
 				</td>
 			</tr>
 				@endforeach
 				@endif
 			</tbody>
 			
 		</table>
 			
 			
 		</div>
 		
 	</div>
 	
 </div>
 @endsection