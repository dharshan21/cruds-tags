
@extends('layouts.app')
@section('content')
<h1>Edit Task</h1>
<hr>
<form action="{{ route('crud.update',$crud->id)}}" method="post">
@csrf
@method('PATCH');
    <div class="form-group">
        <label for="title">Task Title</label>
        <input type="text" class="form-control" id="taskTitle"  name="title" value="{{ $crud->title}}">
    </div>
    <div class="form-group">
        <label for="description">Task Description</label>
         <textarea type="text" class="form-control"  name="description"  rows="15" cols="135px" > {{ $crud->description }}</textarea>
     </div> 
     <div class="form-group">
        <label for="tags">Task tag</label>
        <input type="text" class="form-control" id="taskTitle"  name="tags" value="{{ $crud->tags}}">
    </div>
   
    <button type="submit" class="btn btn-primary">Update</button>
    <button type="submit" class="btn btn-light">Reset</button>

</form>
@endsection